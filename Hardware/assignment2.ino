// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include <math.h>
InternetButton b = InternetButton();

void setup() {
    b.begin();
    Particle.function("checkNumSides", checkNumSides);
    Particle.function("checkAnswer",checkAnswer);
}

void loop() {
    
    int p1x,p1y,p2x,p2y;
    char strAngle[10];
    //How much are you moving in the x direction? (look at the white text on the board)
    int xValue = b.readX();

    //How about in the y direction?
    int yValue = b.readY();
    
    int angle = atan2(yValue - 0, xValue - 0);
    angle = angle * 180 / M_PI;

    //And the z!
    int zValue = b.readZ();
    char strAll[30];
    char strXValue[10];
    sprintf(strXValue, "%d", angle);
    char strYValue[10];
    sprintf(strYValue, "%d", yValue);
    char strZValue[10];
    sprintf(strZValue, "%d", zValue);

    //strAll = "a" + strXValue + strYValue + strZValue ;
    strcat (strAll, "@");
    strcat (strAll, strXValue) ;
    strcat (strAll, "@");
    strcat (strAll, strYValue) ;
    strcat (strAll, "@");
    strcat (strAll, strZValue) ;
    delay(50);
    
    
    if(b.buttonOn(1))
    {
       
         sprintf(strAngle, "%d", angle);
        
        Particle.publish("anglex",(strAngle), 60, PUBLIC);
        Particle.publish("playerChoice","1", 60, PRIVATE);
        delay(200);
    }
    else if(b.buttonOn(2))
    {
        p2x = b.readX();
        p2y = b.readY();
        
        Particle.publish("playerChoice","2", 60, PRIVATE);
        delay(200);
    }
    else if(b.buttonOn(3))
    {
        Particle.publish("playerChoice","3", 60, PRIVATE);
        delay(200);
    }
    else if(b.buttonOn(4))
    {
        Particle.publish("playerChoice","4", 60, PRIVATE);
        delay(200);
    }

}

int checkAnswer(String cmd) {
  if (cmd == "green") {
    b.allLedsOn(0,255,0);
    delay(2000);
    b.allLedsOff();
  }
  else if (cmd == "red") {
    b.allLedsOn(255,0,0);
    delay(2000);
    b.allLedsOff();
  }
  else {
    // you received an invalid color, so
    // return error code = -1
    return -1;
  }
  // function succesfully finished
  return 1;
}

int checkNumSides(String num)
{
    int count = num.toInt();
    int first = count/10;
    int second = count%10;
    for(int i = 0; i<=first ; i++)
    {
        b.ledOn(i, 17, 255, 0);
    }
    delay(500);
    b.allLedsOff();
    for(int i = 0; i<=second ; i++)
    {
        b.ledOn(i, 255, 255, 0);
    }
    delay(500);
    b.allLedsOff();
    
    
    return 0;
}
