//
//  ViewControllerAngle.swift
//  Assignment2Wearables
//
//  Created by Satinder pal Singh on 2019-11-01.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import Foundation

import Particle_SDK

class ViewControllerAngle: UIViewController {
    
    let USERNAME = "satinderpalsingh95@gmail.com"
    let PASSWORD = "Satti1234"
    
    let DEVICE_ID = "38001b001047363333343437"
    var myPhoton : ParticleDevice?
    
    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imgView.image = UIImage(named: "triangle")
            // Do any additional setup after loading the view.
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
                
            }
            
        } // end login
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToAllEvents(withPrefix: "anglex", handler: {
            (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                //print("got event with data \(event?.data)")
                let choice = (event?.data)!
                print("ALL: \(choice)")
                let angle = (choice as NSString).floatValue
                
                    DispatchQueue.main.async {
                        self.imgView.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
                    }
             
                
                
            }
        })
    }
    
   

}
