//
//  ViewController.swift
//  Assignment2Wearables
//
//  Created by Satinder pal Singh on 2019-11-01.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    let USERNAME = "satinderpalsingh95@gmail.com"
    let PASSWORD = "Satti1234"
    
    let DEVICE_ID = "38001b001047363333343437"
    var myPhoton : ParticleDevice?
    var rand = 0
    var score = 0;
    
    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
       
        // Do any additional setup after loading the view.
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
                
            }
            
        } // end login
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    var count = 0;

    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToAllEvents(withPrefix: "playerChoice", handler: {
            (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                print("got event with data \(event?.data)")
                let choice = (event?.data)!
                if (choice == "1") {
                    
                        self.turnRed()
                }
                else if (choice == "2") {
                    self.turnRed()
                }
                else if (choice == "3" ) {
                    if(self.rand == 3)
                    {
                        self.turnGreen()
                        self.score += 1;
                        DispatchQueue.main.async {
                            
                            self.scoreLabel.text = "Score:\(self.score)"
                        }
                    }
                    else{
                        self.turnRed()
                    }
                    
                }
                else if (choice == "4") {
                    if(self.rand == 4)
                    {
                        self.turnGreen()
                        self.score += 1;
                        
                        DispatchQueue.main.async {
                        
                        self.scoreLabel.text = "Score:\(self.score)"
                        }
                        
                    }
                    else{
                        self.turnRed()
                    }
                }
            }
        })
    }
    func turnGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("checkAnswer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
    }
    
    func turnRed() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("checkAnswer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        
    }
    func showScore() {
        
        let parameters = ["\(score)"]
        var task = myPhoton!.callFunction("checkNumSides", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        
    }
    @IBAction func buttonPressed(_ sender: Any) {
        
        rand = Int.random(in: 3...4)
        
        if(rand == 3)
        {
            imgView.image = UIImage(named: "triangle")!
        }
        else{
            imgView.image = UIImage(named: "rect")!
        }
        showScore()
        
        
        
    }
    
}

